# -*- coding: utf-8 -*-
import os
from unittest import main, TestCase

from .metayaml import read, MetaYamlException, FileNotFound, YamlRecursionError


class TestMetaYaml(TestCase):

    @staticmethod
    def _file_name(filename):
        dirname = os.path.dirname(__file__)
        return os.path.join(dirname, "test_files", filename)

    def test_myaml(self):
        d = read(self._file_name("test.yaml"), {"CWD": os.getcwd(), "join": os.path.join})
        self.assertIn("v1", d["main"]["test1"])
        self.assertNotIn("v3", d["main"]["test1"])
        self.assertEqual(d["main"]["test1"][1]["v2"], {'a': u'a', 'b': u'b'})
        self.assertEqual(d["test_math"], 900.0)
        self.assertEqual(d["test_math_str"], "900.0 sec")

        self.assertEqual(d["test2"], ['v1', {'v2': {'a': 'a', 'b': 'b'}}])
        self.assertEqual(d[10], 20)
        self.assertEqual(d["test_lazy_template"], 9)

    def test_failed_lazy_template(self):
        with self.assertRaises(MetaYamlException):
            # Render template error of test_lazy_template, $(f3*3): 'f3' is undefined
            read(self._file_name("f1.yaml"), {"join": os.path.join})

    def test_multi_file_reading(self):
        files = [self._file_name("test.yaml"), self._file_name("test_multi.yaml")]
        d = read(files, {"join": os.path.join})
        self.assertEqual(d["test_math"], 1500)
        self.assertEqual(d["f3"], 33)

    def test_multi_file_reading_match(self):
        files = [self._file_name("test.yaml"), self._file_name("test_m*.yaml")]
        d = read(files, {"join": os.path.join})
        self.assertEqual(d["test_math"], 1500)
        self.assertEqual(d["f3"], 33)

    def test_asset_reading(self):
        files = ['metayaml:test_files/test_assets.yaml']
        d = read(files, {"join": os.path.join}, ignore_not_existed_files=True)
        self.assertEqual(d["test_math"], 1500)
        self.assertEqual(d["f3"], 33)
        self.assertEqual(d['main']['utc'], 0)  # from dict_update.yaml

    def test_error(self):
        # Render template error of test_lazy_template, $(f3*3): 'f3' is undefined
        d = read(self._file_name("f1.yaml"), {"join": os.path.join}, ignore_errors=True)
        self.assertEqual(d["test_lazy_template"], "$(f3*3)")

    def test_asset_error(self):
        files = ['metayaml:test_files/test_assets.yaml']
        self.assertRaises(FileNotFound, read, files, {"join": os.path.join})

    def test_dict_manipulation(self):
        d = read(self._file_name("dict_update.yaml"), {"join": os.path.join})
        self.assertNotIn("test_f1", d["main"])
        self.assertNotIn("f1", d["main"]["all"])
        self.assertNotIn("f2", d["main"]["all"])
        self.assertEqual(d["main"]["remove_all_from_here"], {8: 8})
        self.assertEqual(d["main"]["test1"], ["v3", "v4", "v5"])

    def test_disable_order_dict(self):
        d = read(self._file_name("test.yaml"), {"CWD": os.getcwd(), "join": os.path.join},
                 disable_order_dict=True)
        self.assertEqual(type(d), dict)

    def test_cp(self):
        d = read(self._file_name("cp.yaml"))
        schedule = d["schedule"]
        self.assertEqual(schedule["nighttask"]["min"], 5)
        self.assertEqual(schedule["nighttask"]["hour"], 0)

        self.assertEqual(schedule["daytask"]["min"], 7)
        self.assertEqual(schedule["daytask"]["hour"], 13)

        self.assertEqual(schedule["monthtask"]["min"], 0)
        self.assertEqual(schedule["monthtask"]["day"], 2)

        self.assertEqual(d["deploy"]["elb"], ["1.1.1.1", "2.2.2.2", "3.3.3.3", "4.4.4.4", "5.5.5.5"])

    def test_not_parsible_defaults(self):
        not_parsible = r"${debian_chroot}:+($(debian_chroot)}\u@\h$"
        d = read(self._file_name("test.yaml"),
                 {"env": {"PS1": not_parsible},
                  "join": os.path.join})
        self.assertEqual(d["env"]["PS1"], not_parsible)

    def test_undefined(self):
        d = read('metayaml:test_files/undefined.yaml')
        self.assertEqual(d, {
            'test_path': 'path/to/path',
            'undefined1': None,
            'undefined2': ['abc', None, None, 123],
            'undefined3': {None: 'def', 'key': 'value', 4: 4},
        })

    def test_recursion_detection(self):
        self.assertRaises(YamlRecursionError, read,
                          'metayaml:test_files/r1.yaml', load_file_only_once=False)

    def test_load_config_twice(self):
        d = read(['metayaml:test_files/test_multi.yaml', 'metayaml:test_files/f3.yaml'])
        self.assertTrue(d['main']['test_f2'])  # Overridden by f2.yaml, f3.yaml do not loaded second time

        d = read(['metayaml:test_files/test_multi.yaml', 'metayaml:test_files/f3.yaml'],
                 load_file_only_once=False)
        self.assertIsNone(d['main']['test_f2'])  # f3.yaml has loaded second time and override this field


if __name__ == '__main__':
    main()
